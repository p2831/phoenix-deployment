# Deployment of phoenix Services

Follwoing figure discussed the architecture of the phoenix platfrom. 

![Alt text](architecture.jpg?raw=true "phoenix platfrom architecture")

## Services

There are three main servies;

```
1. elassandra
2. aplos
3. gateway
```


## Deploy services

Start services in following order;

```
docker-compose up -d elassandra
docker-compose up -d aplos
docker-compose up -d gateway
docker-compose up -d web
```
** After hosting website, it can be reached on `<Ip address of the Vm>:4400`.
** Open `7654` and `4400` port on VM for public

#### 1. put Patient record

```
# request
curl -XPOST "http://localhost:7654/api/Patient" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "Name": "Randeepa Bandara",
  "Age": "24",
  "Gender": "male",
  "Control": "user",
}
'

# reply
{"status":200,"msg":"patient added"}
```


#### 2. get Patient Record 

```
# request
curl -XPOST "http://localhost:7654/api/Patient" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "Name": "Randeepa Bandara",
  "Age": "24",
  "Gender": "male",
  "Control": "user",
}
'

# reply
{,"userName":"test user": "done", "timestamp":"2021-11-07 15:20:20.308"}
```
